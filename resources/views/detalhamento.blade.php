<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.mincss') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <title>Document</title>
</head>
<body>
    <h2>Detalhamento da escola</h2>

    <div class="container">
        <h3>Unidade de ensino {{$dados->nome}}</h3>
        <div class="row">
            <div class="col-3">
                código INEP: {{$dados->inep}} 
            
            </div>
            <div class="col-3">
                CNPJ: {{$dados->cnpj}}
                
            
            </div>       
        </div>
    </div>
</body>
</html>