<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Imovel;

class ImovelController extends Controller
{
    public function listar(Request $request){
        $imoveis = json_encode(Imovel::all());
        return view('lista-imoveis', compact('imoveis'));
    }

    public function exibirDados(Request $request, $codigo){
        $dados = Imovel::where('codigo',$codigo)->first();
        // dd($dados);
        return view('detalhamento', compact('dados'));
    }
}
