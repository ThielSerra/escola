<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repasses extends Model
{
    protected $table = "tblrepasses";
    protected $primaryKey = "codigo_imovel";
    public $timestamps = false;
}
