<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terceirizados extends Model
{
    protected $table = "tblterceirizados";
    protected $primaryKey = "codigo_imovel";
    public $timestamps = false;
}
