<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    protected $table = "tblimovel";
    protected $primaryKey = "codigo_imovel";
    public $timestamps = false;
}
