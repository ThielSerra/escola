<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ImovelController@listar');
Route::get('/dados/{codigo}', 'ImovelController@exibirDados');

// Route::GET('/', 'DadosBDController@carregarArquivo');
Route::GET('/admin', 'DadosBDController@index');
Route::POST('/admin/enviargestor', 'DadosBDController@carregarGestor');
Route::POST('/admin/enviarimovel', 'DadosBDController@carregarImovel');
Route::POST('/admin/enviarterceirizado', 'DadosBDController@carregarTerceirizado');
Route::POST('/admin/enviarrepasse', 'DadosBDController@carregarRepasse');
